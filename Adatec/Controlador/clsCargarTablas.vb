﻿Imports System.Data.SqlClient

Public Class clsCargarTablas

    Public Sub CargarTablaBulkCopy(ByVal Datos As DataTable, ByVal Tabla As String)

        Dim ObjConexion As New SqlConnection(My.MySettings.Default.strConexionBD)
        Dim objCorreo As New clsCorreo

        Try
            Dim bulkCopy As New SqlBulkCopy(ObjConexion)
            ObjConexion.Open()
            bulkCopy.BulkCopyTimeout = 350

            limpiar_Tablas(Tabla)
            bulkCopy.DestinationTableName = "dbo." & Tabla
            bulkCopy.WriteToServer(Datos)

        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al cargar datos a tabla auxiliar: ", ex.Message)
        Finally
            ObjConexion.Close()
        End Try
    End Sub
    Public Sub limpiar_Tablas(Tabla As String)

        Dim ObjConexion As New SqlConnection(My.MySettings.Default.strConexionBD)
        Dim objCorreo As New clsCorreo
        Dim objComando As New SqlCommand
        Dim dsCentroOp As New DataSet
        Dim objDA As New SqlDataAdapter
        objComando.Connection = ObjConexion
        objComando.CommandType = CommandType.StoredProcedure
        objComando.CommandText = "LimpiarTablas"

        Try
            ObjConexion.Open()
            objComando.Parameters.AddWithValue("@NombreTabla", Tabla)
            objComando.ExecuteNonQuery()
        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al limpiar tabla: ", ex.Message)
        Finally
            objComando.Parameters.Clear()
            objComando.Connection.Close()
            ObjConexion.Close()
        End Try
    End Sub

End Class
