﻿Imports System.Data.SqlClient
Public Class clsAplicarMerge

    Public Function AplicarMerge()

        Dim objCorreo As New clsCorreo
        Dim ObjConexion As New SqlConnection(My.MySettings.Default.strConexionBD)
        Dim objComando As New SqlCommand
        Dim dsCentroOp As New DataSet
        Dim objDA As New SqlDataAdapter
        objComando.Connection = ObjConexion
        objComando.CommandType = CommandType.StoredProcedure
        objComando.CommandText = "Sincronizar_Tablas"

        Try
            ObjConexion.Open()
            objComando.ExecuteNonQuery()
        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al combinar tablas auxiliares.", ex.Message)
        Finally
            objComando.Parameters.Clear()
            objComando.Connection.Close()
            ObjConexion.Close()
        End Try
    End Function

End Class
