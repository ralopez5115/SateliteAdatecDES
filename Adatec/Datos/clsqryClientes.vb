﻿Imports System.Data.SqlClient
Public Class clsqryClientes

    Dim objConfiguraciones As New clsConfiguracion
    Dim objCorreo As New clsCorreo
    Public Function QryClientes()
        Try

            '1. Consumir el ws de siesa para consultas
            Dim objSiesa As New wsSiesa.WSUNOEE
            Dim ParametroWS As String = "<?xml version=""1.0"" encoding=""utf-8""?>
            <Consulta>
            <NombreConexion>" & objConfiguraciones.ConexionWsSiesa & "</NombreConexion>
            <IdCia>" & objConfiguraciones.CompaniaUnoEE & "</IdCia>
            <IdProveedor>INTERFACES_Y_SOLUCIONES</IdProveedor>
            <IdConsulta>CONSULTA_WS_CLIENTE</IdConsulta>
            <Usuario>" & objConfiguraciones.UsuarioUnoEE & "</Usuario>
            <Clave>" & objConfiguraciones.ClaveUnoEE & "</Clave>
            <Parametros></Parametros>
            </Consulta>"
            Dim dsResultadoSiesa As DataSet
            dsResultadoSiesa = objSiesa.EjecutarConsultaXML(ParametroWS)

            Dim datos As DataTable = New DataTable("datos")

            Dim Columna1 As DataColumn = New DataColumn()
            Columna1.DataType = System.Type.GetType("System.String")
            Columna1.ColumnName = "Columna1"
            Dim Columna2 As DataColumn = New DataColumn()
            Columna2.DataType = System.Type.GetType("System.String")
            Columna2.ColumnName = "Columna2"
            Dim Columna3 As DataColumn = New DataColumn()
            Columna3.DataType = System.Type.GetType("System.String")
            Columna3.ColumnName = "Columna3"
            Dim Columna4 As DataColumn = New DataColumn()
            Columna4.DataType = System.Type.GetType("System.String")
            Columna4.ColumnName = "Columna4"
            Dim Columna5 As DataColumn = New DataColumn()
            Columna5.DataType = System.Type.GetType("System.String")
            Columna5.ColumnName = "Columna5"
            Dim Columna6 As DataColumn = New DataColumn()
            Columna6.DataType = System.Type.GetType("System.String")
            Columna6.ColumnName = "Columna6"
            Dim Columna7 As DataColumn = New DataColumn()
            Columna7.DataType = System.Type.GetType("System.String")
            Columna7.ColumnName = "Columna7"
            Dim Columna8 As DataColumn = New DataColumn()
            Columna8.DataType = System.Type.GetType("System.String")
            Columna8.ColumnName = "Columna8"
            Dim Columna9 As DataColumn = New DataColumn()
            Columna9.DataType = System.Type.GetType("System.String")
            Columna9.ColumnName = "Columna9"
            Dim Columna10 As DataColumn = New DataColumn()
            Columna10.DataType = System.Type.GetType("System.String")
            Columna10.ColumnName = "Columna10"
            Dim Columna11 As DataColumn = New DataColumn()
            Columna11.DataType = System.Type.GetType("System.String")
            Columna11.ColumnName = "Columna11"
            Dim Columna12 As DataColumn = New DataColumn()
            Columna12.DataType = System.Type.GetType("System.String")
            Columna12.ColumnName = "Columna12"
            Dim Columna13 As DataColumn = New DataColumn()
            Columna13.DataType = System.Type.GetType("System.String")
            Columna13.ColumnName = "Columna13"
            Dim Columna14 As DataColumn = New DataColumn()
            Columna14.DataType = System.Type.GetType("System.String")
            Columna14.ColumnName = "Columna14"
            Dim Columna15 As DataColumn = New DataColumn()
            Columna15.DataType = System.Type.GetType("System.String")
            Columna15.ColumnName = "Columna15"
            Dim Columna16 As DataColumn = New DataColumn()
            Columna16.DataType = System.Type.GetType("System.String")
            Columna16.ColumnName = "Columna16"
            Dim Columna17 As DataColumn = New DataColumn()
            Columna17.DataType = System.Type.GetType("System.String")
            Columna17.ColumnName = "Columna17"
            Dim Columna18 As DataColumn = New DataColumn()
            Columna18.DataType = System.Type.GetType("System.String")
            Columna18.ColumnName = "Columna18"
            Dim Columna19 As DataColumn = New DataColumn()
            Columna19.DataType = System.Type.GetType("System.String")
            Columna19.ColumnName = "Columna19"
            Dim Columna20 As DataColumn = New DataColumn()
            Columna20.DataType = System.Type.GetType("System.String")
            Columna20.ColumnName = "Columna20"
            Dim Columna21 As DataColumn = New DataColumn()
            Columna21.DataType = System.Type.GetType("System.String")
            Columna21.ColumnName = "Columna21"
            Dim Columna22 As DataColumn = New DataColumn()
            Columna22.DataType = System.Type.GetType("System.String")
            Columna22.ColumnName = "Columna22"
            Dim Columna23 As DataColumn = New DataColumn()
            Columna23.DataType = System.Type.GetType("System.String")
            Columna23.ColumnName = "Columna23"
            Dim Columna24 As DataColumn = New DataColumn()
            Columna24.DataType = System.Type.GetType("System.String")
            Columna24.ColumnName = "Columna24"
            Dim Columna25 As DataColumn = New DataColumn()
            Columna25.DataType = System.Type.GetType("System.String")
            Columna25.ColumnName = "Columna25"
            Dim Columna26 As DataColumn = New DataColumn()
            Columna26.DataType = System.Type.GetType("System.String")
            Columna26.ColumnName = "Columna26"
            Dim Columna27 As DataColumn = New DataColumn()
            Columna27.DataType = System.Type.GetType("System.String")
            Columna27.ColumnName = "Columna27"
            Dim Columna28 As DataColumn = New DataColumn()
            Columna28.DataType = System.Type.GetType("System.String")
            Columna28.ColumnName = "Columna28"
            Dim Columna29 As DataColumn = New DataColumn()
            Columna29.DataType = System.Type.GetType("System.String")
            Columna29.ColumnName = "Columna29"
            Dim Columna30 As DataColumn = New DataColumn()
            Columna30.DataType = System.Type.GetType("System.String")
            Columna30.ColumnName = "Columna30"
            Dim Columna31 As DataColumn = New DataColumn()
            Columna31.DataType = System.Type.GetType("System.String")
            Columna31.ColumnName = "Columna31"
            Dim Columna32 As DataColumn = New DataColumn()
            Columna32.DataType = System.Type.GetType("System.String")
            Columna32.ColumnName = "Columna32"
            Dim Columna33 As DataColumn = New DataColumn()
            Columna33.DataType = System.Type.GetType("System.String")
            Columna33.ColumnName = "Columna33"

            datos.Columns.Add(Columna1)
            datos.Columns.Add(Columna2)
            datos.Columns.Add(Columna3)
            datos.Columns.Add(Columna4)
            datos.Columns.Add(Columna5)
            datos.Columns.Add(Columna6)
            datos.Columns.Add(Columna7)
            datos.Columns.Add(Columna8)
            datos.Columns.Add(Columna9)
            datos.Columns.Add(Columna10)
            datos.Columns.Add(Columna11)
            datos.Columns.Add(Columna12)
            datos.Columns.Add(Columna13)
            datos.Columns.Add(Columna14)
            datos.Columns.Add(Columna15)
            datos.Columns.Add(Columna16)
            datos.Columns.Add(Columna17)
            datos.Columns.Add(Columna18)
            datos.Columns.Add(Columna19)
            datos.Columns.Add(Columna20)
            datos.Columns.Add(Columna21)
            datos.Columns.Add(Columna22)
            datos.Columns.Add(Columna23)
            datos.Columns.Add(Columna24)
            datos.Columns.Add(Columna25)
            datos.Columns.Add(Columna26)
            datos.Columns.Add(Columna27)
            datos.Columns.Add(Columna28)
            datos.Columns.Add(Columna29)
            datos.Columns.Add(Columna30)
            datos.Columns.Add(Columna31)
            datos.Columns.Add(Columna32)
            datos.Columns.Add(Columna33)

            'LLENAR DESDE LA BASE DE DATOS el DATATABLE
            Dim fila As DataRow
            For Each FilaSiesa As DataRow In dsResultadoSiesa.Tables(0).Rows
                fila = datos.NewRow()

                fila("Columna1") = FilaSiesa.Item("EMPRESA_CODIGO")
                fila("Columna2") = FilaSiesa.Item("CLIENTE_CODIGO_REAL")
                fila("Columna3") = FilaSiesa.Item("CLIENTE_CODIGO")
                fila("Columna4") = FilaSiesa.Item("CLIENTE_TIPO")
                fila("Columna5") = FilaSiesa.Item("CLIENTE_NOMBRE")
                fila("Columna6") = FilaSiesa.Item("CLIENTE_CONTACTO")
                fila("Columna7") = FilaSiesa.Item("CLIENTE_TELEFONO")
                fila("Columna8") = FilaSiesa.Item("CLIENTE_EMAIL")
                fila("Columna9") = FilaSiesa.Item("CLIENTE_DIRECCION")
                fila("Columna10") = FilaSiesa.Item("CLIENTE_CIUDAD")
                fila("Columna11") = FilaSiesa.Item("CLIENTE_REGION")
                fila("Columna12") = FilaSiesa.Item("CLIENTE_PAIS")
                fila("Columna13") = FilaSiesa.Item("CLIENTE_CUPO")
                fila("Columna14") = FilaSiesa.Item("CLIENTE_FORMA_PAGO")
                fila("Columna15") = FilaSiesa.Item("CLIENTE_FORMA_PAGO_CODIGO")
                fila("Columna16") = FilaSiesa.Item("CLIENTE_PLAZO")
                fila("Columna17") = FilaSiesa.Item("LISTA_CODIGO")
                fila("Columna18") = FilaSiesa.Item("CLIENTE_ESTADO")
                fila("Columna19") = FilaSiesa.Item("UBICACION")
                fila("Columna20") = FilaSiesa.Item("CLIENTE_EMAIL2")
                fila("Columna21") = FilaSiesa.Item("DESCUENTO")
                fila("Columna22") = FilaSiesa.Item("LISTA_DESCUENTO")
                fila("Columna23") = FilaSiesa.Item("FUENTE")
                fila("Columna24") = FilaSiesa.Item("NIT")
                fila("Columna25") = FilaSiesa.Item("SUCURSAL")
                fila("Columna26") = FilaSiesa.Item("PUNTO_ENVIO")
                fila("Columna27") = FilaSiesa.Item("COMPANIA")
                fila("Columna28") = FilaSiesa.Item("RN")
                fila("Columna29") = FilaSiesa.Item("CENTRO_OPERACION")
                fila("Columna30") = FilaSiesa.Item("OBSERVACIONES")
                fila("Columna31") = FilaSiesa.Item("CLIENTE_NOMBRE_EST")
                fila("Columna32") = FilaSiesa.Item("CLIENTE_NOMBRE_CORTO")
                fila("Columna33") = FilaSiesa.Item("BLOQUEAR_VENTA_CLIENTE")

                datos.Rows.Add(fila)

            Next

            Dim objBuklCopy As New clsCargarTablas
            objBuklCopy.CargarTablaBulkCopy(datos, "SOURCE_CLIENTES_ERP")
            'objBuklCopy.CargarTablaBulkCopy(datos, "CLIENTES_ERP")
        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al cargar información de clientes: ", ex.Message)
        End Try
    End Function

End Class
