﻿Imports System.Data.SqlClient
Public Class clsqryPrecios

    Dim objConfiguraciones As New clsConfiguracion
    Dim objCorreo As New clsCorreo
    Public Function QryPrecios()
        Try
            '1. Consumir el ws de siesa para consultas
            Dim objSiesa As New wsSiesa.WSUNOEE
            Dim ParametroWS As String = "<?xml version=""1.0"" encoding=""utf-8""?>
            <Consulta>
            <NombreConexion>" & objConfiguraciones.ConexionWsSiesa & "</NombreConexion>
            <IdCia>" & objConfiguraciones.CompaniaUnoEE & "</IdCia>
            <IdProveedor>INTERFACES_Y_SOLUCIONES</IdProveedor>
            <IdConsulta>CONSULTA_WS_PRECIOS</IdConsulta>
            <Usuario>" & objConfiguraciones.UsuarioUnoEE & "</Usuario>
            <Clave>" & objConfiguraciones.ClaveUnoEE & "</Clave>
            <Parametros></Parametros>
            </Consulta>"
            Dim dsResultadoSiesa As DataSet
            dsResultadoSiesa = objSiesa.EjecutarConsultaXML(ParametroWS)

            Dim datos As DataTable = New DataTable("datos")

            Dim Columna1 As DataColumn = New DataColumn()
            Columna1.DataType = System.Type.GetType("System.String")
            Columna1.ColumnName = "Columna1"
            Dim Columna2 As DataColumn = New DataColumn()
            Columna2.DataType = System.Type.GetType("System.String")
            Columna2.ColumnName = "Columna2"
            Dim Columna3 As DataColumn = New DataColumn()
            Columna3.DataType = System.Type.GetType("System.String")
            Columna3.ColumnName = "Columna3"
            Dim Columna4 As DataColumn = New DataColumn()
            Columna4.DataType = System.Type.GetType("System.String")
            Columna4.ColumnName = "Columna4"
            Dim Columna5 As DataColumn = New DataColumn()
            Columna5.DataType = System.Type.GetType("System.String")
            Columna5.ColumnName = "Columna5"
            Dim Columna6 As DataColumn = New DataColumn()
            Columna6.DataType = System.Type.GetType("System.String")
            Columna6.ColumnName = "Columna6"
            Dim Columna7 As DataColumn = New DataColumn()
            Columna7.DataType = System.Type.GetType("System.String")
            Columna7.ColumnName = "Columna7"
            Dim Columna8 As DataColumn = New DataColumn()
            Columna8.DataType = System.Type.GetType("System.String")
            Columna8.ColumnName = "Columna8"
            Dim Columna9 As DataColumn = New DataColumn()
            Columna9.DataType = System.Type.GetType("System.String")
            Columna9.ColumnName = "Columna9"
            Dim Columna10 As DataColumn = New DataColumn()
            Columna10.DataType = System.Type.GetType("System.String")
            Columna10.ColumnName = "Columna10"
            Dim Columna11 As DataColumn = New DataColumn()
            Columna11.DataType = System.Type.GetType("System.String")
            Columna11.ColumnName = "Columna11"
            Dim Columna12 As DataColumn = New DataColumn()
            Columna12.DataType = System.Type.GetType("System.String")


            datos.Columns.Add(Columna1)
            datos.Columns.Add(Columna2)
            datos.Columns.Add(Columna3)
            datos.Columns.Add(Columna4)
            datos.Columns.Add(Columna5)
            datos.Columns.Add(Columna6)
            datos.Columns.Add(Columna7)
            datos.Columns.Add(Columna8)
            datos.Columns.Add(Columna9)
            datos.Columns.Add(Columna10)
            datos.Columns.Add(Columna11)


            'LLENAR DESDE LA BASE DE DATOS el DATATABLE
            Dim fila As DataRow

            For Each FilaSiesa As DataRow In dsResultadoSiesa.Tables(0).Rows
                fila = datos.NewRow()

                fila("Columna1") = FilaSiesa.Item("EMPRESA_CODIGO")
                fila("Columna2") = FilaSiesa.Item("PRODUCTO_CODIGO")
                fila("Columna3") = FilaSiesa.Item("LISTA_CODIGO")
                fila("Columna4") = FilaSiesa.Item("PRECIO")
                fila("Columna5") = FilaSiesa.Item("PRECIO_MINIMO")
                fila("Columna6") = FilaSiesa.Item("PRECIO_BASE")
                fila("Columna7") = FilaSiesa.Item("PRECIO_MAXIMO")
                fila("Columna8") = FilaSiesa.Item("DESCUENTO_MAXIMO")
                fila("Columna9") = FilaSiesa.Item("DESCUENTO_MAXIMO2")
                fila("Columna10") = FilaSiesa.Item("DESCUENTO_MAXIMO3")
                fila("Columna11") = FilaSiesa.Item("VALOR_CARGOS")



                datos.Rows.Add(fila)

            Next
            Dim objBuklCopy As New clsCargarTablas
            objBuklCopy.CargarTablaBulkCopy(datos, "SOURCE_PRECIOS_ERP")
        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al cargar información de precios: ", ex.Message)
        End Try
    End Function

End Class
