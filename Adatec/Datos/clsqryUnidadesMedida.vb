﻿Imports System.Data.SqlClient
Public Class clsqryUnidadesMedida

    Dim objConfiguraciones As New clsConfiguracion
    Dim objCorreo As New clsCorreo
    Public Function QryUnidadesMedida()

        Try

            '1. Consumir el ws de siesa para consultas
            Dim objSiesa As New wsSiesa.WSUNOEE
            Dim ParametroWS As String = "<?xml version=""1.0"" encoding=""utf-8""?>
            <Consulta>
            <NombreConexion>" & objConfiguraciones.ConexionWsSiesa & "</NombreConexion>
            <IdCia>" & objConfiguraciones.CompaniaUnoEE & "</IdCia>
            <IdProveedor>IS</IdProveedor>
            <IdConsulta>CONSULTA_WS_UMED</IdConsulta>
            <Usuario>" & objConfiguraciones.UsuarioUnoEE & "</Usuario>
            <Clave>" & objConfiguraciones.ClaveUnoEE & "</Clave>
            <Parametros></Parametros>
            </Consulta>"
            Dim dsResultadoSiesa As DataSet
            dsResultadoSiesa = objSiesa.EjecutarConsultaXML(ParametroWS)

            Dim datos As DataTable = New DataTable("datos")

            Dim Columna1 As DataColumn = New DataColumn()
            Columna1.DataType = System.Type.GetType("System.String")
            Columna1.ColumnName = "Columna1"
            Dim Columna2 As DataColumn = New DataColumn()
            Columna2.DataType = System.Type.GetType("System.String")
            Columna2.ColumnName = "Columna2"
            Dim Columna3 As DataColumn = New DataColumn()
            Columna3.DataType = System.Type.GetType("System.String")
            Columna3.ColumnName = "Columna3"
            Dim Columna4 As DataColumn = New DataColumn()
            Columna4.DataType = System.Type.GetType("System.String")
            Columna4.ColumnName = "Columna4"
            Dim Columna5 As DataColumn = New DataColumn()
            Columna5.DataType = System.Type.GetType("System.String")
            Columna5.ColumnName = "Columna5"
            Dim Columna6 As DataColumn = New DataColumn()
            Columna6.DataType = System.Type.GetType("System.String")
            Columna6.ColumnName = "Columna6"
            Dim Columna7 As DataColumn = New DataColumn()
            Columna7.DataType = System.Type.GetType("System.String")
            Columna7.ColumnName = "Columna7"
            Dim Columna8 As DataColumn = New DataColumn()
            Columna8.DataType = System.Type.GetType("System.String")
            Columna8.ColumnName = "Columna8"
            Dim Columna9 As DataColumn = New DataColumn()
            Columna9.DataType = System.Type.GetType("System.String")
            Columna9.ColumnName = "Columna9"

            datos.Columns.Add(Columna1)
            datos.Columns.Add(Columna2)
            datos.Columns.Add(Columna3)
            datos.Columns.Add(Columna4)
            datos.Columns.Add(Columna5)
            datos.Columns.Add(Columna6)
            datos.Columns.Add(Columna7)
            datos.Columns.Add(Columna8)
            datos.Columns.Add(Columna9)


            'LLENAR DESDE LA BASE DE DATOS el DATATABLE
            Dim fila As DataRow
            For Each FilaSiesa As DataRow In dsResultadoSiesa.Tables(0).Rows
                fila = datos.NewRow()

                fila("Columna1") = FilaSiesa.Item("f120_rowid")
                fila("Columna2") = FilaSiesa.Item("f120_referencia")
                fila("Columna3") = FilaSiesa.Item("f120_descripcion")
                fila("Columna4") = FilaSiesa.Item("f120_id_unidad_inventario")
                fila("Columna5") = FilaSiesa.Item("f120_id_unidad_adicional")
                fila("Columna6") = FilaSiesa.Item("f122_id_unidad")
                fila("Columna7") = FilaSiesa.Item("f122_factor")
                fila("Columna8") = FilaSiesa.Item("f122_peso")
                fila("Columna9") = FilaSiesa.Item("f122_volumen")

                datos.Rows.Add(fila)

            Next

            Dim objBuklCopy As New clsCargarTablas
            objBuklCopy.CargarTablaBulkCopy(datos, "UNIDADES_MEDIDA_ERP")
        Catch ex As Exception
            MessageBox.Show("Error:" + ex.ToString)
            objCorreo.EnviarCorreoTarea("Error al cargar información de unidades de medida: ", ex.Message)
        End Try

    End Function


End Class
