﻿Imports System.Data.SqlClient
Public Class clsqryInventario

    Dim objConfiguraciones As New clsConfiguracion
    Dim objCorreo As New clsCorreo
    Public Function QryInventario()
        Try

            '1. Consumir el ws de siesa para consultas
            Dim objSiesa As New wsSiesa.WSUNOEE
            Dim ParametroWS As String = "<?xml version=""1.0"" encoding=""utf-8""?>
            <Consulta>
            <NombreConexion>" & objConfiguraciones.ConexionWsSiesa & "</NombreConexion>
            <IdCia>" & objConfiguraciones.CompaniaUnoEE & "</IdCia>
            <IdProveedor>INTERFACES_Y_SOLUCIONES</IdProveedor>
            <IdConsulta>CONSULTA_WS_INVENTARIO</IdConsulta>
            <Usuario>" & objConfiguraciones.UsuarioUnoEE & "</Usuario>
            <Clave>" & objConfiguraciones.ClaveUnoEE & "</Clave>
            <Parametros></Parametros>
            </Consulta>"
            Dim dsResultadoSiesa As DataSet
            dsResultadoSiesa = objSiesa.EjecutarConsultaXML(ParametroWS)


            Dim datos As DataTable = New DataTable("datos")

            Dim Columna1 As DataColumn = New DataColumn()
            Columna1.DataType = System.Type.GetType("System.String")
            Columna1.ColumnName = "Columna1"
            Dim Columna2 As DataColumn = New DataColumn()
            Columna2.DataType = System.Type.GetType("System.String")
            Columna2.ColumnName = "Columna2"
            Dim Columna3 As DataColumn = New DataColumn()
            Columna3.DataType = System.Type.GetType("System.String")
            Columna3.ColumnName = "Columna3"
            Dim Columna4 As DataColumn = New DataColumn()
            Columna4.DataType = System.Type.GetType("System.String")
            Columna4.ColumnName = "Columna4"


            datos.Columns.Add(Columna1)
            datos.Columns.Add(Columna2)
            datos.Columns.Add(Columna3)
            datos.Columns.Add(Columna4)


            'LLENAR DESDE LA BASE DE DATOS el DATATABLE
            Dim fila As DataRow
            For Each FilaSiesa As DataRow In dsResultadoSiesa.Tables(0).Rows
                fila = datos.NewRow()

                fila("Columna1") = FilaSiesa.Item("EMPRESA_CODIGO")
                fila("Columna2") = FilaSiesa.Item("PRODUCTO_CODIGO")
                fila("Columna3") = FilaSiesa.Item("CANTIDAD")
                fila("Columna4") = FilaSiesa.Item("UBICACION")



                datos.Rows.Add(fila)

            Next

            Dim objBuklCopy As New clsCargarTablas
            objBuklCopy.CargarTablaBulkCopy(datos, "SOURCE_INVENTARIO_ERP")
        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al cargar información de Inventarios: ", ex.Message)
        End Try

    End Function

End Class
