﻿Imports System.Data.SqlClient
Public Class clsqryCartera

    Dim objConfiguraciones As New clsConfiguracion
    Dim objCorreo As New clsCorreo
    Public Function QryCartera()
        Try
            '1. Consumir el ws de siesa para consultas
            Dim objSiesa As New wsSiesa.WSUNOEE
            Dim ParametroWS As String = "<?xml version=""1.0"" encoding=""utf-8""?>
            <Consulta>
            <NombreConexion>" & objConfiguraciones.ConexionWsSiesa & "</NombreConexion>
            <IdCia>" & objConfiguraciones.CompaniaUnoEE & "</IdCia>
            <IdProveedor>INTERFACES_Y_SOLUCIONES</IdProveedor>
            <IdConsulta>CONSULTA_WS_CARTERA</IdConsulta>
            <Usuario>" & objConfiguraciones.UsuarioUnoEE & "</Usuario>
            <Clave>" & objConfiguraciones.ClaveUnoEE & "</Clave>
            <Parametros></Parametros>
            </Consulta>"
            Dim dsResultadoSiesa As DataSet
            dsResultadoSiesa = objSiesa.EjecutarConsultaXML(ParametroWS)


            Dim datos As DataTable = New DataTable("datos")

            Dim Columna1 As DataColumn = New DataColumn()
            Columna1.DataType = System.Type.GetType("System.String")
            Columna1.ColumnName = "Columna1"
            Dim Columna2 As DataColumn = New DataColumn()
            Columna2.DataType = System.Type.GetType("System.String")
            Columna2.ColumnName = "Columna2"
            Dim Columna3 As DataColumn = New DataColumn()
            Columna3.DataType = System.Type.GetType("System.String")
            Columna3.ColumnName = "Columna3"
            Dim Columna4 As DataColumn = New DataColumn()
            Columna4.DataType = System.Type.GetType("System.String")
            Columna4.ColumnName = "Columna4"
            Dim Columna5 As DataColumn = New DataColumn()
            Columna5.DataType = System.Type.GetType("System.String")
            Columna5.ColumnName = "Columna5"
            Dim Columna6 As DataColumn = New DataColumn()
            Columna6.DataType = System.Type.GetType("System.String")
            Columna6.ColumnName = "Columna6"
            Dim Columna7 As DataColumn = New DataColumn()
            Columna7.DataType = System.Type.GetType("System.String")
            Columna7.ColumnName = "Columna7"
            Dim Columna8 As DataColumn = New DataColumn()
            Columna8.DataType = System.Type.GetType("System.String")
            Columna8.ColumnName = "Columna8"
            Dim Columna9 As DataColumn = New DataColumn()
            Columna9.DataType = System.Type.GetType("System.String")
            Columna9.ColumnName = "Columna9"
            Dim Columna10 As DataColumn = New DataColumn()
            Columna10.DataType = System.Type.GetType("System.String")
            Columna10.ColumnName = "Columna10"
            Dim Columna11 As DataColumn = New DataColumn()
            Columna11.DataType = System.Type.GetType("System.String")
            Columna11.ColumnName = "Columna11"
            Dim Columna12 As DataColumn = New DataColumn()
            Columna12.DataType = System.Type.GetType("System.String")
            Columna12.ColumnName = "Columna12"
            Dim Columna13 As DataColumn = New DataColumn()
            Columna13.DataType = System.Type.GetType("System.String")
            Columna13.ColumnName = "Columna13"
            Dim Columna14 As DataColumn = New DataColumn()
            Columna14.DataType = System.Type.GetType("System.String")
            Columna14.ColumnName = "Columna14"
            Dim Columna15 As DataColumn = New DataColumn()
            Columna15.DataType = System.Type.GetType("System.String")
            Columna15.ColumnName = "Columna15"
            Dim Columna16 As DataColumn = New DataColumn()
            Columna16.DataType = System.Type.GetType("System.String")
            Columna16.ColumnName = "Columna16"
            datos.Columns.Add(Columna1)
            datos.Columns.Add(Columna2)
            datos.Columns.Add(Columna3)
            datos.Columns.Add(Columna4)
            datos.Columns.Add(Columna5)
            datos.Columns.Add(Columna6)
            datos.Columns.Add(Columna7)
            datos.Columns.Add(Columna8)
            datos.Columns.Add(Columna9)
            datos.Columns.Add(Columna10)
            datos.Columns.Add(Columna11)
            datos.Columns.Add(Columna12)
            datos.Columns.Add(Columna13)
            datos.Columns.Add(Columna14)
            datos.Columns.Add(Columna15)
            datos.Columns.Add(Columna16)

            'LLENAR DESDE LA BASE DE DATOS el DATATABLE
            Dim fila As DataRow
            For Each FilaSiesa As DataRow In dsResultadoSiesa.Tables(0).Rows
                fila = datos.NewRow()

                fila("Columna1") = FilaSiesa.Item("EMPRESA_CODIGO")
                fila("Columna2") = FilaSiesa.Item("CLIENTE_CODIGO")
                fila("Columna3") = FilaSiesa.Item("VENDEDOR_CODIGO")
                fila("Columna4") = FilaSiesa.Item("COBRADOR_CODIGO")
                fila("Columna5") = FilaSiesa.Item("TIPO_DOCUMENTO")
                fila("Columna6") = FilaSiesa.Item("NUMERO_DOCUMENTO")
                fila("Columna7") = FilaSiesa.Item("FECHA_DOCUMENTO")
                fila("Columna8") = FilaSiesa.Item("FECHA_VENCIMIENTO")
                fila("Columna9") = FilaSiesa.Item("SALDO")
                fila("Columna10") = FilaSiesa.Item("OBSERVACIONES")
                fila("Columna11") = FilaSiesa.Item("BASE_RETENCION")
                fila("Columna12") = FilaSiesa.Item("VALOR_IMPUESTOS")
                fila("Columna13") = FilaSiesa.Item("SALDO_BASE")
                fila("Columna14") = FilaSiesa.Item("CUENTA_CONTABLE")
                fila("Columna15") = FilaSiesa.Item("ESTADO")
                fila("Columna16") = FilaSiesa.Item("ID")
                datos.Rows.Add(fila)
            Next

            Dim objBuklCopy As New clsCargarTablas
            objBuklCopy.CargarTablaBulkCopy(datos, "SOURCE_CARTERA_ERP")
        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Error al cargar información de cartera: ", ex.Message)
        End Try
    End Function

End Class
