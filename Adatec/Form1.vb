﻿Imports System.Data.SqlClient
Public Class Form1
    Dim objCorreo As New clsCorreo
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            'Ejecutar consulta de Productos.
            Dim objProductos As New clsqryProductos
            objProductos.qryProductos()

            'Ejecutar la consulta de Vendedores.
            Dim objVendedores As New clsqryVendedores
            objVendedores.QryVendedores()
            'objVendedores.QryVendedores_Data()


            'Ejecutar la consulta de Clientes.
            Dim objClientes As New clsqryClientes
            objClientes.QryClientes()

            'Ejecutar la consulta de Precios.
            Dim objPrecios As New clsqryPrecios
            objPrecios.QryPrecios()

            'Ejecutar la consulta de inventarios.
            Dim objInventario As New clsqryInventario
            objInventario.QryInventario()

            'Ejecutar la consulta de formas de pago.
            Dim objFormasPago As New clsqryFormasPago
            objFormasPago.QryFormasPago()

            'Ejecutar la consulta de cartera.
            Dim objCartera As New clsqryCartera
            objCartera.QryCartera()

            'Ejecutar la consulta de Rutas.
            Dim objRutas As New clsqryRutas
            objRutas.QryRutas()

            'Ejecutar merge entre tablas.
            Dim objAplicar As New clsAplicarMerge
            objAplicar.AplicarMerge()

            'objCorreo.EnviarCorreoTarea("Cargada la información a las tablas.", " OK")

        Catch ex As Exception
            objCorreo.EnviarCorreoTarea("Inconsistencia al cargar clases.", ex.Message)
        Finally
            Me.Close()
        End Try
    End Sub

End Class
